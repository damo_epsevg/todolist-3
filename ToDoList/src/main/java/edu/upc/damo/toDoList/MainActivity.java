package edu.upc.damo.toDoList;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MainActivity extends Activity {
    private Boolean b=true;
    private Button boto;
    private ListView listView;
    private List<CharSequence> dades =new ArrayList<CharSequence>();

    private ArrayAdapter<CharSequence> adaptador;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.llista);

        inicialitza();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    private void afegirDada()
    {
        if(b) {
            dades.add("W");        // Podem modificar la font de dades directament
            Toast.makeText(this,"Modificat el model", Toast.LENGTH_LONG).show();;
        }
        else {
            adaptador.add("Y");    // Podem modificar la font de dades a través de l'adapter
            Toast.makeText(this,"Modificat l'adapter", Toast.LENGTH_LONG).show();;
        }
        adaptador.notifyDataSetChanged();
        b = !b;
    }


    private void inicialitza(){

        listView = (ListView) findViewById(R.id.llista);

        adaptador=ArrayAdapter.createFromResource(this,R.array.dadesEstatiques,android.R.layout.simple_list_item_1);

        listView.setAdapter(adaptador);

        boto = (Button) findViewById(R.id.button);
        boto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                afegirDada();
            }
        });



    }
}
